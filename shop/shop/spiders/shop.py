import scrapy
from urllib.parse import quote
from base64 import b64encode

from ..items import GoodItem
from .locators import TITLE_XPATH


class SearchFirstPageTitleSpider(scrapy.Spider):
    name = 'shop_search_first_page_title'
    base_url = 'https://www.pcstore.com.tw'

    def process_keyword(self, keyword):
        if isinstance(keyword, bytes):
            keyword = str(keyword)

        return b64encode(
            quote(keyword).encode('utf-8')
        ).decode('utf-8')

    def __init__(self, keyword=None, *args, **kwargs):
        assert keyword is not None, 'Must have keyword'

        self.keyword = [
            self.process_keyword(_keyword)
            for _keyword in keyword
        ] if isinstance(keyword, list) else self.process_keyword(keyword)

        super().__init__(*args, **kwargs)

    def start_requests(self):
        if not isinstance(self.keyword, list):
            self.keyword = [self.keyword]

        for keyword in self.keyword:
            url = (self.base_url +
                   f'/adm/psearch.htm?store_k_word={keyword}&slt_k_option=1')
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for title in response.xpath(TITLE_XPATH).extract():
            item = GoodItem()
            item['title'] = title
            yield item
