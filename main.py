import argparse
from scrapy.crawler import CrawlerProcess
from shop.shop.spiders.shop import SearchFirstPageTitleSpider


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--keyword', '-k', type=str,
                        nargs='+', help='keyword for search')
    args = parser.parse_args()

    keyword = getattr(args, 'keyword', None) or '便當盒'
    print(f'The search keyword is {keyword}')

    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })
    process.crawl(SearchFirstPageTitleSpider, keyword=keyword)
    process.start()
