## Install
```shell
python3 -m venv venv
source venv/bin/activate
```

## Usage
### Use `main.py`
```shell
# Use default search string `便當盒`
python3 main.py

# Use the customize keyword
python3 main.py -k TOEIC
# It can be a list
python3 main.py -k Python TOEIC
# It can use the quote as a single string for once search
python3 main.py -k 'TOEIC TEST' Python
```
### Use `Scrapy` command
```shell
# Enter the scrapy.cfg exists folder
cd shop
# Notice: The keyword is required
scrapy crawl shop_search_first_page_title -a keyword='TOEIC'

# You can export as a JSON file
scrapy crawl shop_search_first_page_title -a keyword='TOEIC' -o toeic.json
```
